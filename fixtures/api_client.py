

import pytest

from ApiClient.api_requests import ApiRequests


@pytest.fixture
def check_api_client():
    api_client = ApiRequests()
    yield api_client
    del api_client


