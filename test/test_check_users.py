import requests

from ApiClient.api_requests import ApiRequests


class TestCreateUsers:
    def test_check_users(self, check_api_client: ApiRequests):
        post_req = check_api_client.post_request()
        get_req = check_api_client.get_request()
        assert post_req.ok, f"Request failed with the status code {post_req.status_code}"
        assert get_req.status_code == 200

