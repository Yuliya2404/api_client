import json
import os

import requests as requests


class ApiClient:
    def __init__(self):
        self.__client = requests.Session()

    def post(self, endpoint, body, headers):
        url = f'https://petstore3.swagger.io/api/v3/user/{endpoint}'
        res = self.__client.post(url, json=body, headers=headers)
        return res

    def get(self, endpoint, headers):
        url = f'https://petstore3.swagger.io/api/v3/user/{endpoint}'
        res = self.__client.get(url, headers=headers)
        return res

    def __del__(self):
        self.__client.close()




