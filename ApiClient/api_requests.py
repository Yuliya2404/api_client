import json
import os

import yaml

from ApiClient.api_client import ApiClient
from data.users_deserialization import users_deserialization

class ApiRequests(ApiClient):
    def post_request(self):
        body = users_deserialization(os.path.join('..', 'files', 'users_file.csv'))
        json.dumps(body, indent=4)

        with open(os.path.join("..", "files", "headers.yml")) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        post_req = self.post('createWithList', body, headers)
        return post_req

    def get_request(self):
        with open(os.path.join("..", "files", "headers.yml")) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        get_req = self.get('Mariia24', headers)
        return get_req



        
